"""SeguroCalifornias URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django_registration.backends.one_step.views import RegistrationView

from core.views import IndexTemplateView
from users.forms import CustomUserForm
from users.views import change_password

urlpatterns = [
    path('admin/', admin.site.urls),
    path("accounts/register/",
        RegistrationView.as_view(
            form_class=CustomUserForm,
            success_url="/",
        ), name="django_registration_register"),
    path("accounts/",
        include("django_registration.backends.one_step.urls")),
    path("accounts/",
        include("django.contrib.auth.urls")),
    path("accounts/change-password/", change_password, name="change_password"),
    path("", IndexTemplateView.as_view(), name="home"),
    path("compania/", include("company.urls"), name="cmp"),
    path("clientes/", include("client.urls"), name="cliente"),
    path("seguros/", include("insurance.urls"), name="seguro"),
    path("reaseguros/", include("reinsurance.urls"), name="reaseguro"),
    path("siniestros/", include("sinister.urls"), name="siniestro")
]
