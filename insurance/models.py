from django.db import models

from company.models import CurrencyType
from reinsurance.models import ReinsuranceCoverage, ReinsuranceRisk


class Coverage(models.Model):
    nombre = models.CharField(max_length=50)
    moneda = models.ForeignKey(CurrencyType, on_delete=models.CASCADE)
    prima = models.CharField(max_length=50)
    riesgo_reas = models.ForeignKey(ReinsuranceRisk, on_delete=models.CASCADE)
    cob_reas = models.ForeignKey(ReinsuranceCoverage, on_delete=models.CASCADE, null=True, blank=True)
    tasa_cob = models.FloatField(null=True, blank=True)
    prima_cob = models.FloatField(null=True, blank=True)
    suma_aseg_cob = models.FloatField(null=True, blank=True)
    suma_aseg_min = models.FloatField(null=True, blank=True)
    suma_aseg_max = models.FloatField(null=True, blank=True)
    suma_aseg_min_int = models.FloatField(null=True, blank=True)
    suma_aseg_sin = models.FloatField(null=True, blank=True)
    edad_min = models.IntegerField(null=True, blank=True)
    edad_max = models.IntegerField(null=True, blank=True)
    per_esp_an = models.IntegerField(null=True, blank=True)
    per_esp_mes = models.IntegerField(null=True, blank=True)
    porc_gast_adq = models.FloatField(null=True, blank=True)
    estado = models.BooleanField(default=True)
    por_ant = models.FloatField(null=True, blank=True)
    mnt_max_ant = models.FloatField(null=True, blank=True)
    porc_deduc = models.FloatField(null=True, blank=True)
    mnt_deduc = models.FloatField(null=True, blank=True)
    copago = models.FloatField(null=True, blank=True)
    porc_copago = models.FloatField(null=True, blank=True)
    cob_basica = models.BooleanField(default=False)
    dev_prima = models.BooleanField(default=False)
    apl_factor_x = models.BooleanField(default=False)
    apl_sami = models.BooleanField(default=False)
    acu_sumrs = models.BooleanField(default=False)
    acu_primrs = models.BooleanField(default=False)

    def __str__(self):
        return self. nombre

    class Meta:
        verbose_name_plural = "Coberturas"


class PlanType(models.Model):
    codigo = models.IntegerField(max_length=3)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self. nombre)

    class Meta:
        verbose_name_plural = "Tipos de Planes"


class InsuranceMod(models.Model):
    codigo = models.IntegerField(max_length=3)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Modalidad del Seguroa"


class PolicyPlan(models.Model):
    codigo = models.IntegerField(max_length=3)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Tipos Plan de Polizaa"


class CommercialLevel(models.Model):
    codigo = models.IntegerField(max_length=2)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Niveles Comerciales"


class Plan(models.Model):

    PLAN_CHOICES=(
        ("General", "General"),
        ("Especifico", "Especifico"),
    )

    DURATION_CHOICES = (
        ("Anual", "Anual"),
        ("Corto Plazo", "Corto Plazo"),
    )

    codigo = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    tipo_plan = models.CharField(choices=PLAN_CHOICES, max_length=50, default="General")
    moneda = models.ForeignKey(CurrencyType, on_delete=models.CASCADE)
    dias_retro = models.IntegerField()
    tiṕos_plan = models.ForeignKey(PlanType, on_delete=models.CASCADE)
    mod_seg = models.ForeignKey(InsuranceMod, on_delete=models.CASCADE)
    tipo_pol = models.ForeignKey(PolicyPlan, on_delete=models.CASCADE)
    cod_tempo = models.CharField(choices=DURATION_CHOICES, max_length=50, default="Anual")
    niv_com = models.ForeignKey(CommercialLevel, on_delete=models.CASCADE)
    porc_gast_adm = models.FloatField()
    porc_utilidad = models.FloatField()
    porc_max_comi = models.FloatField()
    plazo_acept_sin = models.IntegerField()
    indisput = models.IntegerField()
    porc_gastos_adq = models.FloatField()
    durac_contra = models.IntegerField()
    estado = models.BooleanField(default=True)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)
    cobert = models.ManyToManyField(Coverage)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.codigo = self.codigo.upper()
        super(Plan, self).save()

    class Meta:
        verbose_name_plural = "Planes"


class PlanTree(models.Model):
    codigo = models.IntegerField(max_length=3)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Planes (Ramos)"


class PayPlans(models.Model):
    codigo = models.IntegerField(max_length=5)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Plan de Pagos"


class Insurance(models.Model):
    CONT_CHOICES = (
        ("Anticipada", "Anticipada"),
        ("Devengada", "Devengada"),
    )

    RISK_CHOICES = (
        ("Alto", "Alto"),
        ("Bajo", "Bajo"),
    )
    codigo = models.CharField(max_length=7)
    nombre = models.CharField(max_length=50)
    tipo_cont = models.CharField(choices=CONT_CHOICES, max_length=50, default="Anticipada")
    dias_canc = models.IntegerField()
    cod_tipo_plan = models.ForeignKey(PlanTree, on_delete=models.CASCADE)
    plan_pago = models.ForeignKey(PayPlans, on_delete=models.CASCADE)
    tipo_riesgo = models.CharField(choices=RISK_CHOICES, max_length=50, default="Alto")
    estado = models.BooleanField(default=True)
    renov_aut = models.BooleanField(default=False)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)
    plan = models.ManyToManyField(Plan)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.codigo = self.codigo.upper()
        super(Insurance, self).save()

    class Meta:
        verbose_name_plural = "Seguros"
