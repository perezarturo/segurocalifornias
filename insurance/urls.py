from django.urls import path

from .views import (CreateCoverageView, CreateInsuranceView, CreatePlanView,
                    DetailCoverageView, DetailInsuranceView, DetailPlanView,
                    ShowCoverageView, ShowInsuranceView, ShowPlanView,
                    UpdateCoverageView)

app_name = "cov"
urlpatterns = [
    path('coberturas/', ShowCoverageView.as_view(), name="lista-cov"),
    path('coberturas/<int:id>/', DetailCoverageView.as_view(), name="detail-cov"),
    path('coberturas/<int:id>/editar/', UpdateCoverageView.as_view(), name="update-cov"),
    path('coberturas/agregar/', CreateCoverageView.as_view(), name="crear-cov"),
    path('planes/', ShowPlanView.as_view(), name="lista-plan"),
    path('planes/<int:id>/', DetailPlanView.as_view(), name="detail-plan"),
    path('planes/agregar/', CreatePlanView.as_view(), name="crear-plan"),
    path('', ShowInsuranceView.as_view(), name="lista-ins"),
    path('<int:id>/', DetailInsuranceView.as_view(), name="detail-ins"),
    path('agregar/', CreateInsuranceView.as_view(), name="crear-ins"),
]
