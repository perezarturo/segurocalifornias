# Generated by Django 3.0.5 on 2020-05-06 17:52

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0003_auto_20200505_1152'),
    ]

    operations = [
        migrations.CreateModel(
            name='PayPlans',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.IntegerField(max_length=5)),
                ('nombre', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Plan de Pagos',
            },
        ),
        migrations.CreateModel(
            name='PlanTree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.IntegerField(max_length=3)),
                ('nombre', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Planes (Ramos)',
            },
        ),
        migrations.CreateModel(
            name='Insurance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.IntegerField(max_length=7)),
                ('nombre', models.CharField(max_length=50)),
                ('tipo_cont', models.CharField(choices=[('Anticipada', 'Anticipada'), ('Devengada', 'Devengada')], default='Anticipada', max_length=50)),
                ('dias_canc', models.IntegerField()),
                ('tipo_riesgo', models.CharField(choices=[('Alto', 'Alto'), ('Bajo', 'Bajo')], default='Alto', max_length=50)),
                ('estado', models.BooleanField(default=True)),
                ('renov_aut', models.BooleanField(default=False)),
                ('fc', models.DateTimeField(auto_now_add=True)),
                ('fm', models.DateTimeField(auto_now=True)),
                ('cod_tipo_plan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insurance.PlanTree')),
                ('plan', models.ManyToManyField(to='insurance.Plan')),
                ('plan_pago', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insurance.PayPlans')),
            ],
            options={
                'verbose_name_plural': 'Seguros',
            },
        ),
    ]
