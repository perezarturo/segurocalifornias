from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from .forms import CoverageForm, InsuranceForm, PlanForm
from .models import Coverage, Insurance, Plan


class ShowCoverageView(LoginRequiredMixin, ListView):
    model = Coverage
    template_name = "coberturas/index.html"
    context_object_name = "obj"
    login_url = "login"


class DetailCoverageView(LoginRequiredMixin, DetailView):
    model = Coverage
    template_name = "coberturas/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class CreateCoverageView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Coverage
    template_name = "coberturas/create.html"
    form_class = CoverageForm
    success_url = reverse_lazy('cov:lista-cov')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "La Cobertura Ha Sido Creada Exitosamente"

    def form_invalid(self, form):
        print(form.errors)
        return self.render_to_response(self.get_context_data(form=form))

    # def form_valid(self, form):
    #     risk = form.save(commit=False)
    #     risk.save()
    #
    #     success_url = reverse('cov:lista-cov')
    #     success_message = "La Cobertura Ha Sido Creada Exitosamente"
    #
    #     return HttpResponseRedirect(success_url, success_message)


class UpdateCoverageView(LoginRequiredMixin, UpdateView):
    model = Coverage
    form_class = CoverageForm
    template_name = "coberturas/update.html"
    pk_url_kwarg = "id"
    context_object_name = 'cli'
    login_url = "login"
    success_url = reverse_lazy('cov:lista-cov')


class ShowPlanView(LoginRequiredMixin, ListView):
    model = Plan
    template_name = "planes/index.html"
    context_object_name = "obj"
    login_url = "login"


class DetailPlanView(LoginRequiredMixin, DetailView):
    model = Plan
    template_name = "planes/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class CreatePlanView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Plan
    template_name = "planes/create.html"
    form_class = PlanForm
    success_url = reverse_lazy('cov:lista-plan')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Plan Ha Sido Creado Exitosamente"


class ShowInsuranceView(LoginRequiredMixin, ListView):
    model = Insurance
    template_name = "seguros/index.html"
    context_object_name = "obj"
    login_url = "login"


class DetailInsuranceView(LoginRequiredMixin, DetailView):
    model = Insurance
    template_name = "seguros/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class CreateInsuranceView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Insurance
    template_name = "seguros/create.html"
    form_class = InsuranceForm
    success_url = reverse_lazy('cov:lista-ins')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Seguro Ha Sido Creado Exitosamente"
