from django.contrib import admin

from .models import (CommercialLevel, Coverage, Insurance, InsuranceMod,
                     PayPlans, Plan, PlanTree, PlanType, PolicyPlan)

admin.site.register(Coverage)
admin.site.register(PlanType)
admin.site.register(InsuranceMod)
admin.site.register(PolicyPlan)
admin.site.register(CommercialLevel)
admin.site.register(Plan)
admin.site.register(PlanTree)
admin.site.register(PayPlans)
admin.site.register(Insurance)
