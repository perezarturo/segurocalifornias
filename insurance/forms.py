from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Column, Layout, MultiField, Row, Submit
from django import forms
from django.forms.widgets import CheckboxSelectMultiple

from .models import Coverage, Insurance, Plan


class CoverageForm(forms.ModelForm):

    class Meta:
        model = Coverage
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CoverageForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('estado', css_class='form-group col-md-12 text-right '),
            ),
            Row(
                Column('nombre', css_class='form-group col-md-12'),
            ),
            Row(
                Column('moneda', css_class='form-group col-md-2'),
                Column('prima', css_class='form-group col-md-2'),
                Column('riesgo_reas', css_class='form-group col-md-3'),
                Column('cob_reas', css_class='form-group col-md-5'),
                css_class='form-row'
            ),
            HTML("<h5>Opciones</h5>"),
            Row(
                Column('tasa_cob', css_class='form-group col-md-4'),
                Column('prima_cob', css_class='form-group col-md-4'),
                Column('suma_aseg_cob', css_class='form-group col-md-4'),
            ),
            Row(
                Column('suma_aseg_min', css_class='form-group col-md-3'),
                Column('suma_aseg_max', css_class='form-group col-md-3'),
                Column('suma_aseg_min_int', css_class='form-group col-md-3'),
                Column('suma_aseg_sin', css_class='form-group col-md-3'),
            ),
            HTML("<h5>Opciones</h5>"),
            Row(
                Column('edad_min', css_class='form-group col-md-2'),
                Column('edad_max', css_class='form-group col-md-2'),
                Column('per_esp_an', css_class='form-group col-md-2'),
                Column('per_esp_mes', css_class='form-group col-md-2'),
                Column('porc_gast_adq', css_class='form-group col-md-4'),
            ),
            HTML("<h5>Opciones</h5>"),
            Row(
                Column('por_ant', css_class='form-group col-md-2'),
                Column('mnt_max_ant', css_class='form-group col-md-2'),
                Column('porc_deduc', css_class='form-group col-md-2'),
                Column('mnt_deduc', css_class='form-group col-md-2'),
                Column('copago', css_class='form-group col-md-2'),
                Column('porc_copago', css_class='form-group col-md-2'),
            ),
            HTML("<h5>Opciones</h5>"),
            Row(
                Column('cob_basica', css_class='form-group col-md-12'),
                Column('dev_prima', css_class='form-group col-md-12'),
                Column('apl_factor_x', css_class='form-group col-md-12'),
                Column('apl_sami', css_class='form-group col-md-12'),
                Column('acu_sumrs', css_class='form-group col-md-12'),
                Column('acu_primrs', css_class='form-group col-md-12'),
            ),
        )
        self.fields['riesgo_reas'].label = "Riesgo de Reaseguro"
        self.fields['cob_reas'].label = "Cobro de Reaseguro"
        self.fields['tasa_cob'].label = "Tasa de Cobertura"
        self.fields['prima_cob'].label = "Prima de Cobertura"
        self.fields['suma_aseg_cob'].label = "Suma Asegurada de Cobertura"
        self.fields['suma_aseg_min'].label = "Suma Asegurada Minima"
        self.fields['suma_aseg_max'].label = "Suma Asegurada Maxima"
        self.fields['suma_aseg_min_int'].label = "Suma Asegurada Minima Int."
        self.fields['suma_aseg_sin'].label = "Suma Asegurada Alerta Siniestros"
        self.fields['suma_aseg_min'].label = "Suma Asegurada Minima"


class PlanForm(forms.ModelForm):

    class Meta:
        model = Plan
        exclude = ['fc', 'fm']

    def __init__(self, *args, **kwargs):
        super(PlanForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('estado', css_class='form-group col-md-12 text-right')
            ),
            Row(
                Column('codigo', css_class='form-group col-md-2'),
                Column('nombre', css_class='form-group col-md-10'),
            ),
            Row(
                Column('tipo_plan', css_class='form-group col-md-4'),
                Column('moneda', css_class='form-group col-md-4'),
                Column('dias_retro', css_class='form-group col-md-4'),
            ),
            Row(
                Column('tiṕos_plan', css_class='form-group col-md-2'),
                Column('mod_seg', css_class='form-group col-md-3'),
                Column('tipo_pol', css_class='form-group col-md-2'),
                Column('cod_tempo', css_class='form-group col-md-2'),
                Column('niv_com', css_class='form-group col-md-3'),
            ),
            Row(
                Column('porc_gast_adm', css_class='form-group col-md-3'),
                Column('porc_utilidad', css_class='form-group col-md-2'),
                Column('porc_max_comi', css_class='form-group col-md-2'),
                Column('plazo_acept_sin', css_class='form-group col-md-3'),
                Column('indisput', css_class='form-group col-md-2'),
                Column('porc_gastos_adq', css_class='form-group col-md-3'),
                Column('durac_contra', css_class='form-group col-md-2')
            ),
            Row(
                Column('cobert', css_class='form-group col-md-12'),
            )
        )
        self.fields['cobert'].label = "Coberturas"
        self.fields['cobert'].widget = CheckboxSelectMultiple()
        self.fields['cobert'].queryset = Coverage.objects.all()
        self.fields['tipo_plan'].label = "Tipo de Plan"
        self.fields['dias_retro'].label = "Dias de Retroactivo"
        self.fields['tiṕos_plan'].label = "SubPlan"
        self.fields['mod_seg'].label = "Modalidad del Seguro"
        self.fields['tipo_pol'].label = "Tipo de Poliza"
        self.fields['cod_tempo'].label = "Duracion"
        self.fields['niv_com'].label = "Nivel Comercial"
        self.fields['porc_gast_adm'].label = "% Gastos de Administracion"
        self.fields['porc_utilidad'].label = "% Utilidad"
        self.fields['porc_max_comi'].label = "% Maximo Comision"
        self.fields['plazo_acept_sin'].label = "Plazo Aceptacion Siniestros"
        self.fields['indisput'].label = "Indisputabilidad"
        self.fields['porc_gastos_adq'].label = "% Gastos Adquisicion"
        self.fields['durac_contra'].label = "Duracion Contrato"


class InsuranceForm(forms.ModelForm):

    class Meta:
        model = Insurance
        exclude = ['fc', 'fm']

    def __init__(self, *args, **kwargs):
        super(InsuranceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column('estado', css_class='form-group col-md-12 text-right')
            ),
            Row(
                Column('codigo', css_class='form-group col-md-2'),
                Column('nombre', css_class='form-group col-md-8'),
                Column('tipo_cont', css_class='form-group col-md-2'),
            ),
            Row(
                Column('dias_canc', css_class='form-group col-md-2'),
                Column('cod_tipo_plan', css_class='form-group col-md-2'),
                Column('plan_pago', css_class='form-group col-md-2'),
                Column('tipo_riesgo', css_class='form-group col-md-2'),
            ),
            Row(
                Column('renov_aut', css_class='form-group col-md-4'),
            ),
            Row(
                Column('plan', css_class='form-group col-md-12'),
            )

        )
        self.fields['tipo_cont'].label = "Tipo de Contabilidad"
        self.fields['dias_canc'].label = "Dias Cancelacion"
        self.fields['cod_tipo_plan'].label = "Tipo de Plan"
        self.fields['plan_pago'].label = "Plan de Pago"
        self.fields['tipo_riesgo'].label = "Tipo de Riesgo"
        self.fields['renov_aut'].label = "Renovacion Automatica de Poliza"
        self.fields['plan'].label = "Planes"
        self.fields['plan'].widget = CheckboxSelectMultiple()
        self.fields['plan'].queryset = Plan.objects.all()
