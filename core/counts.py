from client.models import Client
from insurance.models import Coverage, Insurance, Plan
from reinsurance.models import (ReinsuranceContracts, ReinsuranceCoverage,
                                ReinsuranceRisk)
from sinister.models import Concept, Medicine, MedicineGroup, Transaction


def objects_count():

    counters = {}

    counters['clients_total'] = Client.objects.filter().count()
    counters['plans_total'] = Plan.objects.filter().count()
    counters['coverages_total'] = Coverage.objects.filter().count()
    counters['insurances_total'] = Insurance.objects.filter().count()
    counters['rerisks_total'] = ReinsuranceRisk.objects.filter().count()
    counters['recoverages_total'] = ReinsuranceCoverage.objects.filter().count()
    counters['recontracts_total'] = ReinsuranceContracts.objects.filter().count()
    counters['concepts_total'] = Concept.objects.filter().count()
    counters['transactions_total'] = Transaction.objects.filter().count()
    counters['medicines_total'] = MedicineGroup.objects.filter().count()
    counters['medicines_list_total'] = Medicine.objects.filter().count()

    return counters
