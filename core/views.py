from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView

from .counts import objects_count


class IndexTemplateView(LoginRequiredMixin, TemplateView):

    def get_template_names(self):
        template_name = "home/index.html"
        return template_name

    def get_context_data(self, **kwargs):
        context = super(IndexTemplateView, self).get_context_data(**kwargs)
        context.update(objects_count())
        return context
