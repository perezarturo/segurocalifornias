from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from .forms import RiskForm
from .models import ReinsuranceContracts, ReinsuranceCoverage, ReinsuranceRisk


class ShowReinsuranceRisksView(LoginRequiredMixin, ListView):
    model = ReinsuranceRisk
    template_name = "risks/index.html"
    context_object_name = "obj"
    login_url = "login"


class CreateReinsuranceRisksView(LoginRequiredMixin, CreateView):
    model = ReinsuranceRisk
    form_class = RiskForm
    template_name = "risks/create.html"
    login_url = "login"

    def form_valid(self, form):
        risk = form.save(commit=False)
        risk.save()

        success_url = reverse('reins:lista-rein')

        return HttpResponseRedirect(success_url)


class DetailReinsuranceRisksView(LoginRequiredMixin, DetailView):
    model = ReinsuranceRisk
    template_name = "risks/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class UpdateReinsuranceRisksView(LoginRequiredMixin, UpdateView):
    model = ReinsuranceRisk
    form_class = RiskForm
    template_name = "risks/update.html"
    pk_url_kwarg = "id"
    context_object_name = 'cli'
    login_url = "login"

    def form_valid(self, form):
        risk = form.save(commit=False)
        risk.save()

        success_url = reverse('reins:lista-rein')

        return HttpResponseRedirect(success_url)


class ShowReinsuranceCoverageView(LoginRequiredMixin, ListView):
    model = ReinsuranceCoverage
    template_name = "coverages/index.html"
    context_object_name = "obj"
    login_url = "login"


class ShowReinsuranceContractsView(LoginRequiredMixin, ListView):
    model = ReinsuranceContracts
    template_name = "contracts/index.html"
    context_object_name = "obj"
    login_url = "login"
