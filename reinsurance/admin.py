from django.contrib import admin

from .models import *

admin.site.register(ReinsuranceRisk)
admin.site.register(ReinsuranceContracts)
admin.site.register(ReinsuranceCoverage)
