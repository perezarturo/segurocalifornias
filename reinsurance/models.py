from django.db import models


class ReinsuranceRisk(models.Model):
    codigo = models.CharField(max_length=6)
    nombre = models.CharField(max_length=50)
    estado = models.BooleanField(default=False)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}-{}'.format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural="Riesgos de Reaseguros"


class ReinsuranceContracts(models.Model):

    USO_CONTRATO = (
        ("Emsision", "Emision"),
        ("Siniestros", "Siniestros"),
        ("Emsision/Siniestros", "Emsision/Siniestros")
    )

    CLASE_CONTRATO = (
        ("Proporcional", "Proporcional"),
        ("No Proporcional", "No Proporcional"),
    )

    codigo = models.CharField(max_length=10)
    nombre = models.CharField(max_length=100)
    uso = models.CharField(choices=USO_CONTRATO, default="Emsision/Siniestros", max_length=20)
    clase = models.CharField(choices=CLASE_CONTRATO, default="Proporcional", max_length=30)
    cont_ret = models.BooleanField(default=False, blank=True, null=True)
    cont_fac = models.BooleanField(default=False, blank=True, null=True)
    estado = models.BooleanField(default=True)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Tipos de Contratos"


class ReinsuranceCoverage(models.Model):
    codigo = models.CharField(max_length=10)
    nombre = models.CharField(max_length=100)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.codigo, self.nombre)

    class Meta:
        verbose_name_plural = "Grupos de Coberturas "
