from django.urls import path

from .views import (CreateReinsuranceRisksView, DetailReinsuranceRisksView,
                    ShowReinsuranceContractsView, ShowReinsuranceCoverageView,
                    ShowReinsuranceRisksView, UpdateReinsuranceRisksView)

app_name = "reins"
urlpatterns = [
    path('riesgos-reaseguro/', ShowReinsuranceRisksView.as_view(), name="lista-rein"),
    path('riesgos-reaseguro/crear/', CreateReinsuranceRisksView.as_view(), name="create-rein"),
    path('riesgos-reaseguro/detalles/<int:id>/', DetailReinsuranceRisksView.as_view(), name="detail-rein"),
    path('riesgos-reaseguro/editar/<int:id>/', UpdateReinsuranceRisksView.as_view(), name="update-rein"),
    path('coverturas-reaseguro/', ShowReinsuranceCoverageView.as_view(), name="lista-reinco"),
    path('contratos-reaseguro/', ShowReinsuranceContractsView.as_view(), name="lista-reincont"),
]
