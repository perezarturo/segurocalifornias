from django.apps import AppConfig


class ReinsuranceConfig(AppConfig):
    name = 'reinsurance'
