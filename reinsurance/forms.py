from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column, Layout, Row, Submit
from django import forms

from .models import ReinsuranceRisk


class RiskForm(forms.ModelForm):

    class Meta:
        model = ReinsuranceRisk
        fields = ['codigo', 'nombre', 'estado']
        exclude = ['fc', 'fm']
