from django.contrib import admin

from .models import Client, TipoDocOf

admin.site.register(TipoDocOf)
admin.site.register(Client)
