from django.urls import path

from .views import *

app_name = "cliente"
urlpatterns = [
    path('', ShowClientView.as_view(), name="lista-cliente"),
    path('exportar/xls/', export_clients_xls, name="xlsexport-cliente"),
    path('exportar/pdf/', PDFClientView.as_view(), name="pdfexport-cliente"),
    path('exportar/pdf/<int:pk>/', PDFDetailClientView.as_view(), name="pdfexport-cliente-det"),
    path('agregar/', CreateClientView.as_view(), name="agregar-cliente"),
    path('detalles/<int:id>/', DetailClientView.as_view(), name="detail-cliente"),
    path('editar/<int:id>/', UpdateClientView.as_view(), name="update-cliente"),
    path('deshabilitar/<int:id>/', client_disable, name="disable-cliente"),
    path('habilitar/<int:id>/', client_enable, name="enable-cliente"),
]
