from django.db import models


class TipoDocOf(models.Model):
    nombre = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de Documentos Oficiales"


class Client(models.Model):
    nombre = models.CharField(max_length=30)
    apep = models.CharField(max_length=30)
    apem = models.CharField(max_length=30)
    direccion = models.CharField(max_length=150)
    no_int = models.CharField(max_length=10, null=True)
    no_ext = models.CharField(max_length=10)
    colonia = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=20)
    estado = models.CharField(max_length=20)
    cp = models.CharField(max_length=5)
    pais = models.CharField(max_length=20)
    tel = models.CharField(max_length=50)
    email = models.EmailField()
    tipodoc = models.ForeignKey(TipoDocOf, on_delete=models.CASCADE)
    nodocof = models.CharField(max_length=15)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return '{} - {} {} {}'.format(self.id, self.apep, self.apem, self.nombre)

    class Meta:
        verbose_name_plural = "Clientes"
