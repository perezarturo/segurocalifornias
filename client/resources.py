from import_export import resources
from import_export.fields import Field
from import_export.widgets import DateWidget

from .models import Client


class ClientResource(resources.ModelResource):
    id = Field(attribute='id', column_name='ID')
    nombre = Field(attribute='nombre', column_name='Nombre')
    apep = Field(attribute='apep', column_name='Apellido Paterno')
    apem = Field(attribute='apem', column_name='Apellido Materno')
    direccion = Field(attribute='direccion', column_name='Direccion')
    no_int = Field(attribute='no_int', column_name='No. Interior')
    no_ext = Field(attribute='no_ext', column_name='No. Exterior')
    colonia = Field(attribute='colonia', column_name='Colonia')
    ciudad = Field(attribute='ciudad', column_name='Ciudad')
    cp = Field(attribute='cp', column_name='Codigo Postal')
    pais = Field(attribute='pais', column_name='Pais')
    tel = Field(attribute='tel', column_name='Telefono')
    email = Field(attribute='email', column_name='Correo')
    tipodoc = Field(attribute='tipodoc', column_name='Documento Of.')
    nodocof = Field(attribute='nodocof', column_name='No. Doc. Of.')
    fc = Field(attribute='fc', column_name='Fecha Creacion', widget=DateWidget(format='%d-%m-%Y'))
    estado = Field(attribute='estado', column_name='Estado')

    class Meta:
        model = Client
        exclude = {'fm', }
