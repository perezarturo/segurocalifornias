from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column, Layout, Row, Submit
from django import forms

from .models import Client


class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = ['nombre', 'apep', 'apem', 'direccion', 'no_int', 'no_ext', 'colonia', 'ciudad', 'cp',
                  'pais', 'tel', 'email', 'tipodoc', 'nodocof', 'estado']

    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('nombre', css_class='form-group col-md-4'),
                Column('apep', css_class='form-group col-md-4'),
                Column('apem', css_class='form-group col-md-4'),
                css_class='form-row'
            ),
            'direccion',
            Row(
                Column('no_int', css_class='form-group col-md-2'),
                Column('no_ext', css_class='form-group col-md-2'),
                Column('colonia', css_class='form-group col-md-2'),
                Column('ciudad', css_class='form-group col-md-2'),
                Column('cp', css_class='form-group col-md-2'),
                Column('pais', css_class='form-group col-md-2'),
                css_class='form-row'
            ),
            Row(
                Column('tel', css_class='form-group col-md-6'),
                Column('email', css_class='form-group col-md-6'),
                css_class='form-row'
            ),
            Row(
                Column('tipodoc', css_class='form-group col-md-6'),
                Column('nodocof', css_class='form-group col-md-6'),
                css_class='form-row'
            ),
            'estado'
        )
        self.fields['nombre'].label = "Nombre"
        self.fields['apep'].label = "Apellido Paterno"
        self.fields['apem'].label = "Apellido Materno"
        self.fields['no_int'].label = "No. Interior"
        self.fields['no_ext'].label = "No. Exterior"
        self.fields['cp'].label = "Codigo Postal"
        self.fields['tel'].label = "Telefono"
        self.fields['tipodoc'].label = "Documento Oficial"
        self.fields['nodocof'].label = "No. del Documento Oficial"
