from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.template.loader import get_template, render_to_string
from django.urls import reverse
from django.utils import timezone
from django.views.generic import (CreateView, DetailView, ListView, UpdateView,
                                  View)

from .forms import ClientForm
from .models import Client
from .render import Render
from .resources import ClientResource


class ShowClientView(LoginRequiredMixin, ListView):
    model = Client
    template_name = "clientes/index.html"
    context_object_name = "obj"
    login_url = "login"


def export_clients_xls(request):
    client_resource = ClientResource()
    dataset = client_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Clientes.xls"'

    return response


class PDFClientView(LoginRequiredMixin, View):
    login_url = "login"

    def get(self, request):
        clients = Client.objects.all()
        today = timezone.now()
        params = {
            'today': today,
            'clients': clients,
            'request': request
        }
        return Render.render("clientes/pdfdetail.html", params)


class PDFDetailClientView(LoginRequiredMixin, View):
    login_url = "login"

    def get(self, request, pk):
        clientdet = Client.objects.get(id=pk)
        today = timezone.now()
        params = {
            'today': today,
            'clientdet': clientdet,
            'request': request
        }
        return Render.render("clientes/pdfdetails.html", params)


class CreateClientView(LoginRequiredMixin, CreateView):
    model = Client
    form_class = ClientForm
    template_name = 'clientes/create.html'
    login_url = "login"

    def form_valid(self, form):
        cli = form.save(commit=False)
        cli.save()

        success_url = reverse('cliente:lista-cliente')

        return HttpResponseRedirect(success_url)


class DetailClientView(LoginRequiredMixin, DetailView):
    model = Client
    template_name = "clientes/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class UpdateClientView(LoginRequiredMixin, UpdateView):
    model = Client
    template_name = "clientes/update.html"
    form_class = ClientForm
    pk_url_kwarg = "id"
    context_object_name = 'cli'
    login_url = "login"

    def form_valid(self, form):
        cli = form.save(commit=False)
        cli.save()

        success_url = reverse('cliente:lista-cliente')

        return HttpResponseRedirect(success_url)


def client_disable(request, id):
    cliente = Client.objects.filter(pk=id).first()
    contexto = {}
    template_name = "clientes/disable.html"

    if not cliente:
        return redirect("cliente:lista-cliente")

    if request.method == 'GET':
        contexto = {'obj': cliente}

    if request.method == 'POST':
        cliente.estado = False
        cliente.save()
        messages.success(request, 'Cliente Deshabilitado')
        return redirect("cliente:lista-cliente")

    return render(request, template_name, contexto)


def client_enable(request, id):
    cliente = Client.objects.filter(pk=id).first()
    contexto = {}
    template_name = "clientes/enable.html"

    if not cliente:
        return redirect("cliente:lista-cliente")

    if request.method == 'GET':
        contexto = {'obj': cliente}

    if request.method == 'POST':
        cliente.estado = True
        cliente.save()
        messages.success(request, 'Cliente Habilitado')
        return redirect("cliente:lista-cliente")

    return render(request, template_name, contexto)
