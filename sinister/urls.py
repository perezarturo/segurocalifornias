from django.urls import path

from .views import (CreateConceptView, CreateMedicineGroupView,
                    CreateMedicineView, CreateTransactionView,
                    DetailMedicineGroupView, DetailMedicineView,
                    PDFMedicineView, ShowConceptView, ShowMedicineGroupView,
                    ShowMedicineView, ShowTransactionView,
                    UpdateMedicineGroupView, UpdateMedicineView,
                    export_medicinegroup_xls, export_medicinelist_xls)

app_name = "sin"
urlpatterns = [
    path('conceptos/', ShowConceptView.as_view(), name="lista-concept"),
    path('conceptos/crear/', CreateConceptView.as_view(), name="crear-concept"),
    path('transacciones/', ShowTransactionView.as_view(), name="lista-trans"),
    path('transacciones/crear/', CreateTransactionView.as_view(), name="crear-trans"),
    path('medicamentos/', ShowMedicineGroupView.as_view(), name="lista-medi"),
    path('medicamentos/crear/', CreateMedicineGroupView.as_view(), name="crear-medi"),
    path('medicamentos/editar/<int:id>/', UpdateMedicineGroupView.as_view(), name="update-medi"),
    path('medicamentos/<int:id>/', DetailMedicineGroupView.as_view(), name="detail-medi"),
    path('exportar/xls/', export_medicinegroup_xls, name="xlsexport-medi"),
    path('exportar/pdf/', PDFMedicineView.as_view(), name="pdfexport-medi"),
    path('medicamentos/lista/', ShowMedicineView.as_view(), name="lista-medilist"),
    path('medicamentos/lista/exportar/xls/', export_medicinelist_xls, name="xlsexport-medilist"),
    path('medicamentos/lista/crear/', CreateMedicineView.as_view(), name="crear-medilist"),
    path('medicamentos/lista/<int:id>', DetailMedicineView.as_view(), name="detail-medilist"),
    path('medicamentos/lista/editar/<int:id>/', UpdateMedicineView.as_view(), name="update-medilist"),
]
