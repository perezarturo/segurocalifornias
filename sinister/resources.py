from import_export.fields import Field
from import_export.resources import ModelResource
from import_export.widgets import ManyToManyWidget

from .models import Medicine, MedicineGroup


class MedicineResource(ModelResource):
    codigo = Field(attribute='codigo', column_name='Codigo')
    nombre = Field(attribute='nombre', column_name='Nombre')
    precio_max = Field(attribute='precio_max', column_name='Precio Maximo')
    precio_min = Field(attribute='precio_min', column_name='Precio Minimo')
    obsv = Field(attribute='obsv', column_name='Observaciones')
    req_auth = Field(attribute='req_auth', column_name='Requiere Autorizacion')
    auth_med = Field(attribute='auth_med', column_name='Autorizacion Medica')
    auth_med_area = Field(attribute='auth_med_area', column_name='Autorizacion Medico del Area')
    auth_call_center = Field(attribute='auth_call_center', column_name='Autorizacion Call Center')
    medicamento = Field(attribute='medicamento', column_name='Lista Medicamentos',
                        widget=ManyToManyWidget(model=Medicine, field='nombre'))

    class Meta:
        model = MedicineGroup
        exclude = {'id', }


class MedicineListResource(ModelResource):
    codigo_ean = Field(attribute='codigo_ean', column_name='Codigo EAN')
    nombre = Field(attribute='nombre', column_name='Nombre')
    princ_activo = Field(attribute='princ_activo', column_name='Principio Activo')
    precio_max = Field(attribute='precio_max', column_name='Precio Maximo')
    forma = Field(attribute='forma', column_name='Forma')
    presentacion = Field(attribute='presentacion', column_name='Presentacion')
    lab = Field(attribute='lab', column_name='Laboratorio')
    via_admin = Field(attribute='via_admin', column_name='Via Administracion')
    qty = Field(attribute='qty', column_name='Cantidad')
    concentra = Field(attribute='concentra', column_name='Concentracion')
    um = Field(attribute='um', column_name='Unidad de Medida')
    indicaciones = Field(attribute='indicaciones', column_name='Indicaciones')
    contra = Field(attribute='contra', column_name='ContraIndicaciones')

    class Meta:
        model = Medicine
        exclude = {'id', 'fc', 'fm'}
