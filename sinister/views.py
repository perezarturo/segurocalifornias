from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import (CreateView, DetailView, ListView, UpdateView,
                                  View)

from client.render import Render

from .forms import (ConceptForm, MedicineForm, MedicineGroupForm,
                    TransactionForm)
from .models import Concept, Medicine, MedicineGroup, Transaction
from .resources import MedicineListResource, MedicineResource


class ShowConceptView(LoginRequiredMixin, ListView):
    model = Concept
    template_name = "conceptos/index.html"
    context_object_name = "obj"
    login_url = "login"


class CreateConceptView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Concept
    template_name = "conceptos/create.html"
    form_class = ConceptForm
    success_url = reverse_lazy('sin:lista-concept')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Concepto Ha Sido Creada Exitosamente"


class ShowTransactionView(LoginRequiredMixin, ListView):
    model = Transaction
    template_name = "transacciones/index.html"
    context_object_name = "obj"
    login_url = "login"


class CreateTransactionView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Transaction
    template_name = "transacciones/create.html"
    form_class = TransactionForm
    success_url = reverse_lazy('sin:lista-trans')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "La Transaccion Ha Sido Creada Exitosamente"


class ShowMedicineView(LoginRequiredMixin, ListView):
    model = Medicine
    template_name = "medicinas/index.html"
    context_object_name = "obj"
    login_url = "login"


def export_medicinelist_xls(request):
    client_resource = MedicineListResource()
    dataset = client_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="ListaMedicamentos".xls"'

    return response


class CreateMedicineView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Medicine
    template_name = "medicinas/create.html"
    form_class = MedicineForm
    success_url = reverse_lazy('sin:lista-medilist')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Medicamento Ha Sido Creado Exitosamente"


class DetailMedicineView(LoginRequiredMixin, DetailView):
    model = Medicine
    template_name = "medicinas/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class UpdateMedicineView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Medicine
    template_name = "medicinas/update.html"
    form_class = MedicineForm
    pk_url_kwarg = "id"
    context_object_name = 'medl'
    success_url = reverse_lazy('sin:lista-medilist')
    login_url = "login"

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Medicamento Ha Sido Actualizado Exitosamente"


class ShowMedicineGroupView(LoginRequiredMixin, ListView):
    model = MedicineGroup
    template_name = "grupomedicinas/index.html"
    context_object_name = "obj"
    login_url = "login"


def export_medicinegroup_xls(request):
    client_resource = MedicineResource()
    dataset = client_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="GrupoMedicamentos.xls"'

    return response


class PDFMedicineView(LoginRequiredMixin, View):
    login_url = "login"

    def get(self, request):
        meds = MedicineGroup.objects.all()
        today = timezone.now()
        params = {
            'today': today,
            'clients': meds,
            'request': request
        }
        return Render.render("grupomedicinas/pdfdetail.html", params)


class CreateMedicineGroupView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = MedicineGroup
    template_name = "grupomedicinas/create.html"
    form_class = MedicineGroupForm
    success_url = reverse_lazy('sin:lista-medi')
    login_url = 'login'

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Grupo de Medicamento Ha Sido Creado Exitosamente"


class DetailMedicineGroupView(LoginRequiredMixin, DetailView):
    model = MedicineGroup
    template_name = "grupomedicinas/detail.html"
    context_object_name = "obj"
    pk_url_kwarg = "id"
    login_url = "login"


class UpdateMedicineGroupView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = MedicineGroup
    template_name = "grupomedicinas/update.html"
    form_class = MedicineGroupForm
    pk_url_kwarg = "id"
    context_object_name = 'med'
    success_url = reverse_lazy('sin:lista-medi')
    login_url = "login"

    def get_success_message(self, cleaned_data):
        print(cleaned_data)
        return "El Grupo de Medicamento Ha Sido Actualizado Exitosamente"
