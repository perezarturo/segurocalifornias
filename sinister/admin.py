from django.contrib import admin

from .models import Concept, Lab, Medicine, MedicineGroup, Transaction

admin.site.register(Concept)
admin.site.register(Transaction)
admin.site.register(Lab)
admin.site.register(Medicine)
admin.site.register(MedicineGroup)
