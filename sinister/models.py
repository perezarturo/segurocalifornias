from django.db import models


class Concept(models.Model):

    CONCEPT_TYPE_CHOICES = {
        ("Cobertura Basica", "Cobertura Basica"),
        ("Cobertura Adicional", "Cobertura Adicional"),
        ("Otros Conceptos", "Otros Conceptos"),
        ("Todos", "Todos"),
    }

    tipo_con = models.CharField(choices=CONCEPT_TYPE_CHOICES, max_length=50, default="Cobertura Basica")
    codigo = models.CharField(max_length=10)
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.codigo = self.codigo.upper()
        super(Concept, self).save()

    class Meta:
        verbose_name_plural = "Conceptos"


class Transaction(models.Model):

    WHERE_CHOICES = {
        ("Reservas de Siniestros", "Reservas de Siniestros"),
        ("Pagos de Siniestros", "Pagos de Siniestros"),
        ("Reservas y Pagos", "Reservas y Pagos"),
    }

    SIGN_CHOICES = {
        ("Positivo", "Positivo"),
        ("Negativo", "Negativo")
    }

    codigo = models.CharField(max_length=10)
    nombre = models.CharField(max_length=50)
    donde_applica = models.CharField(choices=WHERE_CHOICES, max_length=30, default="Reservas de Siniestros")
    signo = models.CharField(choices=SIGN_CHOICES, max_length=20, default="Positivo")
    estado = models.BooleanField(default=True)
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)
    conceptos = models.ManyToManyField(Concept)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.codigo = self.codigo.upper()
        super(Transaction, self).save()

    class Meta:
        verbose_name_plural = "Transacciones"


class Lab(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return "{} - {}".format(self.id, self.nombre)

    class Meta:
        verbose_name_plural = "Laboratorios"


class Medicine(models.Model):

    FORMA_CHOICES = {
        ("Capsulas", "Capsulas"),
        ("Ampolla", "Ampolla"),
        ("Tableta", "Tableta"),
    }

    PRESENTATION_CHOICES = {
        ("Caja", "Caja"),
        ("Bote", "Bote"),
    }

    VIA_CHOICES = {
        ("Oral", "Oral"),
        ("Cutanea", "Cutanea"),
        ("Epidural", "Epidural"),
    }

    codigo_ean = models.CharField(max_length=30)
    nombre = models.CharField(max_length=30)
    princ_activo = models.CharField(max_length=30)
    precio_max = models.FloatField()
    forma = models.CharField(choices=FORMA_CHOICES, max_length=30, default="Capsulas")
    presentacion = models.CharField(choices=PRESENTATION_CHOICES, max_length=30, default="Caja")
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    via_admin = models.CharField(choices=VIA_CHOICES, max_length=30, default="Oral")
    qty = models.IntegerField()
    concentra = models.IntegerField()
    um = models.CharField(max_length=10)
    indicaciones = models.TextField(null=True, blank=True, help_text="Indicaciones del medicamento")
    contra = models.TextField(null=True, blank=True, help_text="Contraindicaciones del medicamento")
    fc = models.DateTimeField(auto_now_add=True)
    fm = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.codigo_ean, self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.codigo_ean = self.codigo_ean.upper()
        super(Medicine, self).save()

    class Meta:
        verbose_name_plural = "Medicamentos"


class MedicineGroup(models.Model):
    codigo = models.CharField(max_length=30)
    nombre = models.CharField(max_length=30)
    precio_max = models.FloatField()
    precio_min = models.FloatField()
    obsv = models.TextField(null=True, blank=True)
    req_auth = models.BooleanField(default=True)
    auth_med = models.BooleanField(default=True)
    auth_med_area = models.BooleanField(default=True)
    auth_call_center = models.BooleanField(default=True)
    medicamento = models.ManyToManyField(Medicine)

    def __str__(self):
        return "{} - {}".format(self.codigo, self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.codigo = self.codigo.upper()
        super(MedicineGroup, self).save()

    class Meta:
        verbose_name_plural = "Grupo de Medicamentos"
