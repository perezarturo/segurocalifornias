from django.apps import AppConfig


class SinisterConfig(AppConfig):
    name = 'sinister'
