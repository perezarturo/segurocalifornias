from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Column, Layout, MultiField, Row, Submit
from django.forms import ModelForm
from django.forms.widgets import CheckboxSelectMultiple

from .models import Concept, Medicine, MedicineGroup, Transaction


class ConceptForm(ModelForm):

    class Meta:
        model = Concept
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(ConceptForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('tipo_con', css_class='form-group col-md-12'),
            ),
            Row(
                Column('codigo', css_class='form-group col-md-3'),
                Column('nombre', css_class='form-group col-md-9'),
            )
        )
        self.fields['tipo_con'].label = "Tipo de Conceptos"


class TransactionForm(ModelForm):

    class Meta:
        model = Transaction
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(TransactionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('estado', css_class='form-group col-md-12 text-right '),
            ),
            Row(
                Column('codigo', css_class='form-group col-md-3'),
                Column('nombre', css_class='form-group col-md-9'),
            ),
            Row(
                Column('donde_applica', css_class='form-group col-md-3'),
                Column('signo', css_class='form-group col-md-3'),
            ),
            Row(
                Column('conceptos', css_class='form-group col-md-12'),
            ),
        )
        self.fields['donde_applica'].label = "Donde Aplica?"
        self.fields['conceptos'].widget = CheckboxSelectMultiple()
        self.fields['conceptos'].queryset = Concept.objects.all()


class MedicineForm(ModelForm):

    class Meta:
        model = Medicine
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(MedicineForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('codigo_ean', css_class='form-group col-md-2'),
                Column('nombre', css_class='form-group col-md-6'),
                Column('princ_activo', css_class='form-group col-md-2'),
                Column('precio_max', css_class='form-group col-md-2'),
            ),
            Row(
                Column('forma', css_class='form-group col-md-2'),
                Column('presentacion', css_class='form-group col-md-2'),
                Column('via_admin', css_class='form-group col-md-2'),
                Column('qty', css_class='form-group col-md-2'),
                Column('concentra', css_class='form-group col-md-2'),
                Column('um', css_class='form-group col-md-2'),
            ),
            Row(
                Column('lab', css_class='form-group col-md-12'),
            ),
            Row(
                Column('indicaciones', css_class='form-group col-md-6'),
                Column('contra', css_class='form-group col-md-6'),
            ),


        )
        self.fields['codigo_ean'].label = "Codigo EAN"
        self.fields['princ_activo'].label = "Principio Activo"
        self.fields['precio_max'].label = "Precio Maximo"
        self.fields['via_admin'].label = "Via Administracion"
        self.fields['qty'].label = "Cantidad"
        self.fields['concentra'].label = "Concentracion"
        self.fields['um'].label = "Unidad de Medida"
        self.fields['lab'].label = "Laboratorio"
        self.fields['contra'].label = "ContraIndicaciones"


class MedicineGroupForm(ModelForm):

    class Meta:
        model = MedicineGroup
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(MedicineGroupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('codigo', css_class='form-group col-md-2'),
                Column('nombre', css_class='form-group col-md-6'),
                Column('precio_min', css_class='form-group col-md-2'),
                Column('precio_max', css_class='form-group col-md-2'),
            ),
            Row(
                Column('obsv', css_class='form-group col-md-12'),
            ),
            Row(
                Column('req_auth', css_class='form-group col-md-3'),
                Column('auth_med', css_class='form-group col-md-3'),
                Column('auth_med_area', css_class='form-group col-md-3'),
                Column('auth_call_center', css_class='form-group col-md-3'),
            ),
            Row(
                Column('medicamento', css_class='form-group col-md-12'),
            )
        )
        self.fields['precio_min'].label = "Precio Minimo"
        self.fields['precio_max'].label = "Precio Maximo"
        self.fields['obsv'].label = "Observaciones"
        self.fields['req_auth'].label = "Requiere Autorizacion"
        self.fields['auth_med'].label = "Autoriza Medico Especialista"
        self.fields['auth_med_area'].label = "Autoriza Area Medica"
        self.fields['auth_call_center'].label = "Autoriza Call Center"
        self.fields['medicamento'].label = "Medicamentos"
        self.fields['medicamento'].widget = CheckboxSelectMultiple()
        self.fields['medicamento'].queryset = Medicine.objects.all()
