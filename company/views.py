from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, ListView

from .models import CompanyInfo, CurrencyType


class ShowCompanyInfo(LoginRequiredMixin, ListView):
    model = CompanyInfo
    template_name = "index.html"
    context_object_name = "obj"
    login_url = "login"


class ShowCurrencyType(LoginRequiredMixin, ListView):
    model = CurrencyType
    template_name = "currency/index.html"
    context_object_name = "obj"
    login_url = "login"
