from django.urls import path

from .views import ShowCompanyInfo, ShowCurrencyType

app_name = "cmp"
urlpatterns = [
    path('datos-empresa/', ShowCompanyInfo.as_view(), name="lista-comp"),
    path('monedas/', ShowCurrencyType.as_view(), name="lista-curr"),
]
