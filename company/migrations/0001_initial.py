# Generated by Django 3.0.5 on 2020-04-28 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=150)),
                ('direccion', models.CharField(max_length=150)),
                ('no_int', models.CharField(max_length=10)),
                ('no_ext', models.CharField(max_length=10)),
                ('colonia', models.CharField(max_length=50)),
                ('ciudad', models.CharField(max_length=20)),
                ('estado', models.CharField(max_length=20)),
                ('cp', models.CharField(max_length=5)),
                ('pais', models.CharField(max_length=20)),
                ('tel', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('rfc', models.CharField(max_length=50)),
            ],
        ),
    ]
