from django.db import models


class CompanyInfo(models.Model):
    nombre = models.CharField(max_length=150)
    direccion = models.CharField(max_length=150)
    no_int = models.CharField(max_length=10)
    no_ext = models.CharField(max_length=10)
    colonia = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=20)
    estado = models.CharField(max_length=20)
    cp = models.CharField(max_length=5)
    pais = models.CharField(max_length=20)
    tel = models.CharField(max_length=50)
    email = models.EmailField()
    rfc = models.CharField(max_length=50)

    def __str__(self):
        return '{}'.format(self.nombre)

    def save(self):
        self.nombre = self.nombre.upper()
        super(CompanyInfo, self).save()

    class Meta:
        verbose_name="Compañia"


class CurrencyType(models.Model):
    codigo = models.CharField(max_length=4)
    descripcion = models.CharField(max_length=15)

    def __str__(self):
        return self.codigo

    class Meta:
        verbose_name_plural = "Monedas y Tipos de Cambios"
