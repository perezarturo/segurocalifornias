from django.contrib import admin

from .models import CompanyInfo, CurrencyType


class InfoCompany(admin.ModelAdmin):
    list_display = ('nombre', 'direccion')


admin.site.register(CompanyInfo, InfoCompany)
admin.site.register(CurrencyType)
admin.site.site_header = "Administracion Seguro de las Californias"
